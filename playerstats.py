import sys
from qlstats import DuelPlayer

weapons = ['mg', 'sg', 'gl', 'rl', 'lg', 'rg', 'pg']
player = DuelPlayer(sys.argv[1])

print player.name
print ''
print 'Elo ' + str(player.elo)
print 'Rank ' + str(player.rank)
print 'Win% ' + str("%i" % (player.winPercentage+0.5)) + "%"
print ''
print 'Most played maps'
print ''
for i in range(3):
    if player.maps[i]:
        print player.maps[i]['name'] + " (" + str("%.2f" % player.maps[i]['winRate']) + "%)"
print ''
print "\tacc\tusage"
for weapon in player.weapons:
    if weapon['name'] in weapons:
        print weapon['name'] + "\t" + str(int(weapon["accuracy"] + 0.5)) + "%\t" + str(int(weapon["usage"] + 0.5)) + "%"
