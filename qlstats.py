import urllib2
import json
from BeautifulSoup import BeautifulSoup

class DuelPlayer:

    URL = "http://www.qlranks.com/duel/player/"

    def __init__(self, name):
        self.name = ""
        self.rank = ""
        self.elo = ""
        self.wins = ""
        self.losses = ""
        self.played = ""
        self.winPercentage = ""
        self.weapons = []
        self.maps = []

        self.loadPlayer(name)

    def __str__(self):
        output = []
        line = "="*20
        output.append(self.name)
        output.append(line)
        output.append("ELO:\t" + str(self.elo))
        output.append("Rank:\t" + str(self.rank))
        output.append("Wins:\t" + str(self.wins))
        output.append("Losses:\t" + str(self.losses))
        output.append("Win%:\t" + str(self.winPercentage))
        output.append("Total:\t" + str(self.played))

        output.append("\nMaps")
        output.append(line)
        for m in self.maps:
            output.append(m['name'] + "\t" + m['played'])

        output.append("\nWeapons")
        output.append(line)
        for weapon in self.weapons:
            output.append(weapon['name'])
            output.append("Frags: " + "\t" + weapon["frags"])
            output.append("Accuracy: " + "\t" + weapon["accuracy"])
            output.append("Usage: " + "\t" + weapon["usage"])


        return "\n".join(output)

    def loadPlayer(self, name):
        page = urllib2.urlopen(self.URL + name)
        s = page.read()
        soup = BeautifulSoup(s)

        self.name = name
        self.elo = int(soup.find("span", {"id":"ctl00_ContentPlaceHolder1_lblPlayerCurrentElo"}).text)
        self.rank = int(soup.find("span", {"id":"ctl00_ContentPlaceHolder1_lblPlayerWorldrank"}).text)
        self.played = int(soup.find("span", {"id":"ctl00_ContentPlaceHolder1_lblDuelsTracked"}).text)
        self.wins = int(soup.find("span", {"id":"ctl00_ContentPlaceHolder1_lblDuelWins"}).text)
        self.losses = int(soup.find("span", {"id":"ctl00_ContentPlaceHolder1_lblVSLosses"}).text)
        self.winPercentage = float(100.0 / self.played * self.wins)

        self.weapons = self.getWeaponStats(soup)
        self.maps = self.getMapStats(soup)

    def getWeaponStats(self, soup):
        data = {
                "names" : [],
                "frags" : [],
                "accuracy" : [],
                "use" : [],
                }

        soup = soup.find("div", {"id":"weapon_stats"}).table
        for s in soup.th.findNextSiblings():
            s = str(s)
            start = s.find("icon ")+5
            data['names'].append(s[start:s.find('"', start)])

        for i in ["frags", "accuracy", "use"]:
            for s in soup.find("tr", {"class":i}).td.findNextSiblings():
                if s.text == "N/A":
                    data[i].append("0.0%")
                else: 
                    data[i].append(str(s.text))

        weapons = []
        for i in range(len(data['names'])):
            weapon = {
                    "name" : data['names'][i],
                    "frags" : int(data['frags'][i]),
                    "accuracy" : float(data['accuracy'][i][:-1]),
                    "usage" : float(data['use'][i][:-1]),
                    }
            weapons.append(weapon)

        return weapons

    def getMapStats(self, soup):
        s = soup.find("div", {"id":"content"}).find("div", {"class":"inner"}).script.text
        s = s[s.find("["):s.find("]")+1].replace('label', '"label"').replace('data', '"data"')
        s = json.loads(s)

        maps = []
        for m in s:
            tmp = {}
            tmp['name'] = str(m['label'])
            tmp['played'] = str(m['data'])
            tmp['winRate'] = 100.0 / self.played * m['data']
            maps.append(tmp)

        return maps
